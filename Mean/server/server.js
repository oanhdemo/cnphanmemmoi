var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require("morgan");
var mongoose = require("mongoose");
var config= require('./config');
var path=require('path');
// const api = require('./app/routes/api');
var cors = require('cors')




//use body so we can grab information from POST request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())


//configure our app to handle cors request
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
})

//log all request to the console
app.use(morgan('dev'));
app.get('/', function (req, res) {
    res.send('Hello')
})


// try to connect to Mongo remotely which post in cloud
mongoose.Promise=global.Promise;
mongoose.connect(config.database,{useNewUrlParser:true,useUnifiedTopology:true});
mongoose.set('useCreateIndex',true);

//used for request that our frontend will make
//app.use(express.static(__dirname+'./public'));

//routes for our API =========

//API route-------------
// var apiRouter=require('./app/routes/api');
// app.use('/api', apiRouter);

const customerRoute = require('./app/routes/customer');
const authenticateRoute = require('./app/routes/authenticate');

app.use('/customers', customerRoute);
app.use('/authenticates', authenticateRoute);

//MAIN CATCALL ROUTE--------------
//SEND USERS TO FONTEND
// has to to register after API ROUTES
// app.get('*',function(req,res){
//     res.sendFile(path.join(__dirname+ '/public/views/index.html'))
// });


//START THE SERVER
//===========
 //check token code stop here 
    // test route to make sure everything is working
    //accessed at GET http://localhost:8080/api
app.listen(config.port);
console.log("http://localhost:" + config.port)


const express = require('express');
const authenticate=require('../middlewares/authenticate');

var router = express.Router();

var customerController = require('../controllers/customer.controller');

router.get('/get/:id',authenticate, customerController.getCustomerByID);
router.get('/get',authenticate, customerController.getCustomer);
router.post('/create',customerController.createCustomer);
router.put('/edit/:id',authenticate, customerController.editCustomer);
router.delete('/delete/:id',authenticate, customerController.deleteCustomerByID);

module.exports = router;

const express = require('express');
var authenticateController = require('../controllers/authenticate.contronller');

var router = express.Router();

router.post('/login',  authenticateController.Login);
router.post('/loginSocial',  authenticateController.LoginSocial);

module.exports = router;

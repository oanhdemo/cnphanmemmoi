var Customer = require("../models/customer");
var jwt = require("jsonwebtoken");
var config = require("../../config");
var express = require("express")();

//super secret for creating token
var superSecret = config.secret;

module.exports = function() {
  var apiRouter = express.Router();

  apiRouter.use(function(req, res, next) {
    console.log("Main");
    next();
  });
  apiRouter.get("/", function(req, res) {
    res.json({ message: "Hello" });
  });

  apiRouter.route("/customers").post(function(req, res) {
    var customer = new Customer();
    customer.name = req.body.name;
    customer.password = req.body.password;
    customer.phoneNumber = req.body.phoneNumber;
    customer.address = req.body.address;
    customer.job = req.body.job;

    customer.save(function(err) {
      if (err) {
        if ((err.code = 11000))
          return res.json({
            success: false,
            message: "phone number already exists",
          });
        else return res.send(err);
      }
      res.json({ message: "Create Successful" });
    });
  });

  apiRouter.route("/customers").get(function(req, res) {
    Customer.find(function(err, customers) {
      if (err) return res.send(err.message);
      res.json(customers);
    });
  });

  apiRouter.route("/customers/:id").get(function(req, res) {
    Customer.findById(req.params.id, function(err, customers) {
      if (err) return res.send(err);
      res.json(customers);
    });
  });

  apiRouter.route("/customers/:id").put(function(req, res) {
    Customer.findById(req.params.id, function(err, customers) {
      if (err) return res.send(err);
      if (req.body.name) customers.name = req.body.name;
      if (req.body.address) customers.name = req.body.address;
      if (req.body.job) customers.name = req.body.job;

      customers.save(function(err) {
        if (err) {
          return res.send(err);
        }
        res.json({ message: "Update Successful" });
      });
    });
  });

  apiRouter.route("/customers/:id").delete(function(req, res) {
    Customer.remove(
      {
        _id: req.params.id,
      },
      function(err, customer) {
        if (err) return res.send(err);
        res.json({ message: "Delete Successful" });
      }
    );
  });

  //route to authenticate a user (Post http://localhost:8080/api/authenticate)
  apiRouter.post("/authenticate", function(req, res) {
    console.log("authenticate");
    Customer.findOne({
      name: req.body.name,
    })
      .select("name username password")
      .exec(function(err, user) {
        if (err) throw err;

        //no user with that username was found
        if (!user) {
          res.json({
            sucess: false,
            message: "Authentication failed.User not found.",
          });
        } else if (user) {
          var valiPassword = user.comparePassword(req.body.password);
          if (!valiPassword) {
            res.json({
              success: false,
              message: "Authentication failed. Wrong password.",
            });
          } else {
            // if user found and password is right
            //create token
            var token = jwt.sign(
              {
                name: user.name,
                //   username:user.username
              },
              superSecret,
              {
                expiresIn: "24h", // expires in 24h
              }
            );
            res.json({
              sucess: true,
              message: "Lam viec voi token",
              token: token,
            });
          }
        }
      });
  });
  //code nay khong cho phep lam gi ma k cos token
  apiRouter.use(function(req, res, next) {
    var token =
      req.body.token || req.query.token || req.headers["x-access-token"];

    //decode token
    if (token) {
      //verify secret and check exp
      jwt.verify(token, superSecret, function(err, decoded) {
        if (err) {
          return res.json({
            success: false,
            message: "Failed to authentication",
          });
        } else {
          // if everything is good , save to request for user in other routes
          req.decoded = decoded;
          next(); // make sure we go to the next and dont stop here
        }
      });
    } else {
      // if there is no token
      //return an HTTP response of 403(access forbidden) and an error message
      return res.status(403).send({
        success: false,
        message: "No token provided.",
      });
    }
  });
};

var Customer = require("../models/customer");

exports.getCustomer = function(req, res, next) {
    Customer.find(function(err, customers) {
        if (err) return res.send(err.message);
        res.json(customers);
    });
}

exports.createCustomer = async function (req, res, next) {
    var customer = new Customer();
    customer.name = req.body.name;
    customer.email=req.body.email;
    customer.password = req.body.password;
    customer.phoneNumber = req.body.phoneNumber;
    customer.address = req.body.address;
    customer.job = req.body.job;
  //  customer.setPassword(req.body.password);


     customer.save(function(err) {
      if (err) {
        if ((err.code = 11000))
          return res.json({
            success: false,
            message: err,
          });
        else return res.send(err);
      }
      res.json({ message: "Create Successful" });
    });
}
exports.editCustomer=function(req,res,next){
    Customer.findById(req.params.id, function(err, customers) {
        if (err) return res.send(err);
        if (req.body.name) customers.name = req.body.name;
        if (req.body.address) customers.address = req.body.address;
        if (req.body.job) customers.job = req.body.job;

        customers.save(function(err) {
          if (err) {
            return res.send(err);
          }
          res.json({ message: "Update Successful" });
        });
      });
}

exports.deleteCustomerByID=function(req,res,next){
    Customer.remove(
        {
          _id: req.params.id,
        },
        function(err, customer) {
          if (err) return res.send(err);
          res.json({ message: "Delete Successful" });
        }
      );
}

exports.getCustomerByID=function(req,res,next){
    Customer.findById(req.params.id, function(err, customers) {
        if (err) return res.send(err);
        res.json(customers);
      });
}

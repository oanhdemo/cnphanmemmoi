var Customer = require("../models/customer");
var config = require("../../config");
var bcrypt = require('bcryptjs');
var superSecret = config.secret;
var jwt = require("jsonwebtoken");
exports.Login =  function(req, res, next) {

    Customer.findOne({
      email: req.body.email,
    })
      .select("name username password")
      .exec(async function(err, user) {
        if (err) throw err;

        //no user with that username was found
        if (!user) {
          res.json({
            sucess: false,
            message: "Authentication failed.User not found.",
          });
        } else if (user) {
          var valiPassword = await bcrypt.compare(req.body.password,user.password);
          console.log("authenticate thu test",valiPassword);
          if (!valiPassword) {
            res.json({
              success: false,
              message: "Authentication failed. Wrong password.",
            });
          } else {
            // if user found and password is right
            //create token
            var token = jwt.sign(
              {
                email: user.email,
                //   username:user.username
              },
              superSecret,
              {
                expiresIn: 180, // expires in 24h
              }
            );
            res.json({
              sucess: true,
              message: "Lam viec voi token",
              token: token,
            });
          }
        }
      });
}

exports.LoginSocial =  function(req, res, next) {

  Customer.findOne({
    email: req.body.email,
  })
    .select("name username password")
    .exec(async function(err, user) {
      if (err) throw err;

      //no user with that username was found
      if (user!=null) {
        var token = jwt.sign(
          {
            email: req.body.email,
            //   username:user.username
          },
          superSecret,
          {
            expiresIn: "3m", // expires in 24h
          }
        );
        res.json({
          sucess: true,
          message: "Lam viec voi token",
          token: token,
        });
      } else {

        res.json({
          sucess: true,
          message: "Email does not exist",
        });
      }
    });
}

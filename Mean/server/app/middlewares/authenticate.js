var jwt = require("jsonwebtoken");
var config = require("../../config");
var superSecret = config.secret;

const authenticate = function(req, res, next) {
  var token = req.body.token || req.query.token || req.headers["x-access-token"];

  //decode token
  if (token) {
    //verify secret and check exp
    jwt.verify(token, superSecret, function(err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: "Failed to authentication",
        });
      } else {
        // if everything is good , save to request for user in other routes
        req.decoded = decoded;
        next(); // make sure we go to the next and dont stop here
      }
    });
  } else {
    // if there is no token
    //return an HTTP response of 403(access forbidden) and an error message
    return res.status(403).send({
      success: false,
      message: "No token provided.",
    });
  }
};
module.exports=authenticate;

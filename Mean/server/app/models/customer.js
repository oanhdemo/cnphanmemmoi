var mongoose = require('mongoose');
var Schema=mongoose.Schema;
var bcrypt = require('bcryptjs');
var crypto = require('crypto');
const { use } = require('passport');
// mongoose.connect('mongodb+srv://Nhocga123:Nhocga123@cluster0.u7vq9.mongodb.net/DemoMongo?retryWrites=true&w=majority', {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// });
var CustomerSchema=new Schema({
    name: {type:String ,required:true},
    email:{type:String},
    password:{type:String},
    salt :{type: String} ,
    phoneNumber: {type:String ,required:true, index:{unique:true}},
    address: {type:String ,required:true},
    job: {type:String},
    provider:{type:String}
})


CustomerSchema.pre('save',async function(next){
  //console.log('hash password')
  const user = this;
  if(user.isModified('password')){

    user.password=await bcrypt.hash(user.password,8)
    console.log('hash password')
    next();
  }
  next();
})




module.exports=mongoose.model('Customer',CustomerSchema);

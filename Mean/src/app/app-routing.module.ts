import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth.guard';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ManagementComponent } from './management/management.component';
import { UserComponent } from './management/user/user.component';
import { CreateSocailComponent } from './login/create-socail/create-socail.component';


const routes: Routes = [
  {path: "", component: LoginComponent},
  {path: "login", component: LoginComponent},
  {path: "createSocail/:email", component: CreateSocailComponent},
  {path: "management", component: ManagementComponent, canActivate: [AuthGuard],
    children: [{path: "user", component: UserComponent}]
  },
  {path: "**", component: LoginComponent},
]

@NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
    })
export class AppRoutingModule { }

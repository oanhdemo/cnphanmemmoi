import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})

export class ManagementComponent implements OnInit {

  isNavbarCLosed = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick() {
    localStorage.removeItem("token");
    this.router.navigate(['/']);
  }

  // roleMatch(allowedRoled): boolean {
  //   var isMatch = false;
  //   var payload = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
  //   var userRole = payload.role;
  //   if (userRole == allowedRoled) {
  //     isMatch = true;
  //   }
  //   return isMatch;
  // }

  onNavbarClose(){
    this.isNavbarCLosed = !this.isNavbarCLosed
  }
}

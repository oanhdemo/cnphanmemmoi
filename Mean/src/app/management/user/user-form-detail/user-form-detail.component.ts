// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../service/user.service';

// @Component({
//   selector: 'app-user-form-detail',
//   templateUrl: './user-form-detail.component.html',
//   styleUrls: ['./user-form-detail.component.scss']
// })
// export class UserFormDetailComponent implements OnInit {

//   constructor(public service: UserService,
//   ) { }


//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-form-detail',
  templateUrl: './user-form-detail.component.html',
  styleUrls: ['./user-form-detail.component.scss']
})
export class UserFormDetailComponent implements OnInit {

  constructor( public service: UserService, 
     private toastr: ToastrService) { }

  ngOnInit() {
    this.service.isCreate=true;
    //this.service.formModel.reset();
  }

  onSubmit() {
    if (this.service.isCreate) {
      this.createUser();
    }
    else {
      this.updateUser(this.service.userId);
    }
    this.service.setFormToCreate();
  }


  createUser() {
    this.service.createUser().subscribe(
      (res: any) => {
        if (res.message == "Create Successful") {
          this.toastr.success('Người dùng được tạo', 'Tạo thành công');
          this.service.formModel.reset();
          this.service.refeshList();
          this.service.isCreate = true;
        }
        else {
          this.toastr.error(res.message, 'Tạo thất bại')
        }
      },
      err => {
        console.log(err)
      }
    );
  }

  updateUser(id) {
    this.service.UpdateUser(id).subscribe(
      (res: any) => {
        if (res.message == "Update Successful") {
          this.toastr.success('Thông tin đã cập nhật', 'Cập nhật thành công');
          this.service.formModel.reset();
          this.service.refeshList();
          this.service.isCreate = true;
        }
        else {
          this.toastr.error(res.message, 'Cập nhật thất bại')
        }
      },
      err => {
        console.log(err)
      }
    );
  }


}


import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ApiUrl } from 'src/app/app.component';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    listUser: any[] = [];
    isCreate = true;
    userId = 0;
    constructor(private fb: FormBuilder, private http: HttpClient) {
    }
    rootURl = ApiUrl;
    formModel = this.fb.group({
        UserName: ['', Validators.required],
        PhoneNumber: ['', Validators.required],
        Email:['',[Validators.email, Validators.required]],
        Address: ['', Validators.required],
        Job: [''],
        Passwords: this.fb.group({
            Password: ['', [Validators.required, Validators.minLength(4), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')]],
            ConfirmPassword: ['', Validators.required]
        }, { validators: this.comparePasswords })
    });

    setFormToUpdate() {
        this.formModel = this.fb.group({
            UserName: ['', Validators.required],
            PhoneNumber: ['', [Validators.required]],
            Address: ['', Validators.required],
            Job: [''],
        });
    }

    setFormToCreate() {
        this.formModel = this.fb.group({
            UserName: ['', Validators.required],
            Email:['',[Validators.email, Validators.required]],
            PhoneNumber: ['', [Validators.required]],
            Address: ['', Validators.required],
            Job: [''],
            Passwords: this.fb.group({
                Password: ['', [Validators.required, Validators.minLength(4), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')]],
                ConfirmPassword: ['', Validators.required]
            }, { validators: this.comparePasswords })
        });
    }

    comparePasswords(fb: FormGroup) {
        let confirmPassctrl = fb.get('ConfirmPassword');
        if (confirmPassctrl.errors == null || 'passwordMismatch' in confirmPassctrl.errors) {
            if (fb.get('Password').value != confirmPassctrl.value)
                confirmPassctrl.setErrors({ passwordMismatch: true })
            else
                confirmPassctrl.setErrors(null);
        }
    }

    createUser() {
        var body = {
            name: this.formModel.value.UserName,
            email:this.formModel.value.Email,
            phoneNumber: this.formModel.value.PhoneNumber,
            address: this.formModel.value.Address,
            job: this.formModel.value.Job,
            password: this.formModel.value.Passwords.Password,
        }
        var url = "/customers/create"
        url = this.rootURl + url;
        return this.http.post(url, body);
    }

    UpdateUser(id) {
        var body = {
            name: this.formModel.value.UserName,
            phoneNumber: this.formModel.value.PhoneNumber,
            address: this.formModel.value.Address,
            job: this.formModel.value.Job
        }
        

        var url = this.rootURl + "/customers/edit/" + id;
        console.log(url)
        return this.http.put(url, body);
    }
    refeshList() {
        this.http.get(this.rootURl + "/customers/get")
            .toPromise()
            .then(async res => {
                this.listUser = await res as any[];
            });
    }

    deleteUser(id) {
        return this.http.delete(this.rootURl + '/customers/delete/' + id);
    }
}
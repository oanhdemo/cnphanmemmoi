import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  p: number = 1;
  searchString: string;

  constructor(public service: UserService,
    private toastr: ToastrService,) { }

  ngOnInit() {
    this.service.refeshList();
  }

  populateForm(user) {
    this.service.setFormToUpdate();
    this.service.userId = user._id;
    this.service.formModel.controls.UserName.setValue(user.name);
    this.service.formModel.controls.PhoneNumber.setValue(user.phoneNumber);
    this.service.formModel.controls.Address.setValue(user.address);
    this.service.formModel.controls.Job.setValue(user.job);
    this.service.isCreate=false;
  }

  onDelete(user) {
    var id = user._id;
    if(confirm("Bạn có chắc muốn xóa hay không?"))
    {       
      this.service.deleteUser(id).subscribe(
        (res:any) => {
          if (res.message == "Delete Successful") {
            this.service.refeshList();
            this.toastr.warning('Xóa thành công', 'Vô hiệu hóa người dùng');
          }
          else {
            //this.toastr.warning('Xóa thành công', 'Vô hiệu hóa người dùng');
          }
        },
        err => {
          console.log(err.message);
        }
      )
    }
  }
}

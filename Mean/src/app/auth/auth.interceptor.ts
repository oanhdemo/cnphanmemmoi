import { HttpHeaders, HttpInterceptor, HttpRequest, HttpHandler,HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from "rxjs/operators";
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(private router:Router){

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
        if(localStorage.getItem('token')!=null)
        {
            const cloneReq=req.clone(
                {headers:req.headers.set('x-access-token',localStorage.getItem('token'))
            });
            return next.handle(cloneReq).pipe(
                tap(
                    res=>{
                        var a:any=res;
                        if(a.body != undefined && a.body.success==false)
                        {
                            localStorage.removeItem('token');
                            this.router.navigateByUrl('/login'); 
                        }
                    },
                    err=>{
                        if(err.status==401){
                        localStorage.removeItem('token');
                        this.router.navigateByUrl('/login');  
                        }
                        if(err.status==403){
                            localStorage.removeItem('token');
                            this.router.navigateByUrl('/login');  
                        }
                    }
                )
            )
        }
        else
            return next.handle(req.clone());
    }


}
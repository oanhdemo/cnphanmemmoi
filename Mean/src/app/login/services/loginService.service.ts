import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiUrl } from 'src/app/app.component';

@Injectable({
  providedIn: 'root'
})

export class loginService {
  url = ApiUrl;
  email:string="";
  formModel = this.fb.group({
    UserName: ['', Validators.required],
    PhoneNumber: ['', Validators.required],
    Email:[''],
    Address: ['', Validators.required],
    Job: [''],
    Passwords: this.fb.group({
        Password: ['', [Validators.required, Validators.minLength(4), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$')]],
        ConfirmPassword: ['', Validators.required]
    }, { validators: this.comparePasswords })
  });
  constructor(private http: HttpClient, private fb: FormBuilder) { }

  Save(response) {
    console.log(response);
  }

  comparePasswords(fb: FormGroup) {
    let confirmPassctrl = fb.get('ConfirmPassword');
    if (confirmPassctrl.errors == null || 'passwordMismatch' in confirmPassctrl.errors) {
        if (fb.get('Password').value != confirmPassctrl.value)
            confirmPassctrl.setErrors({ passwordMismatch: true })
        else
            confirmPassctrl.setErrors(null);
    }
  }

  createUser() {
    var body = {
        name: this.formModel.value.UserName,
        email:this.formModel.value.Email,
        phoneNumber: this.formModel.value.PhoneNumber,
        address: this.formModel.value.Address,
        job: this.formModel.value.Job,
        password: this.formModel.value.Passwords.Password,
    }
    var url = "/customers/create"
    url = this.url + url;
    return this.http.post(url, body);
}

  saveResponse(response){
    return this.http.post(this.url + '/authenticates/loginSocial',response);
  }

  roleMatch(allowedRoled): boolean {
    var isMatch = false;
    var payload = JSON.parse(window.atob(localStorage.getItem('token').split('.')[1]));
    var userRole = payload.role;
    allowedRoled.forEach(element => {
      if (userRole == element) {
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }
  login(formData) {
    var data ={
        email:formData.Email,
        password:formData.Password
    }
    return this.http.post(this.url + '/authenticates/login', data);
  }
}
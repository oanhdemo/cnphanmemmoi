import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, AuthService } from 'angularx-social-login';
import { SocailUsers } from '../login/services/socialUsers.model';
import { ToastrService } from 'ngx-toastr';
import { loginService } from './services/loginService.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formModel = {
    Email: '',
    Password: ''
  }

  response;
  socailUsers;

  constructor(private router: Router,
    private authService: AuthService,
    private loginServices: loginService, private toastr: ToastrService) { }

  ngOnInit() {
    if (localStorage.getItem('token') != null)
      this.router.navigateByUrl('/management')
  }

  public socailSignIn(socailProvider: string) {
    let socialPlatformProvider;
    if (socailProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socailProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    try {
      this.authService.signIn(socialPlatformProvider).then(socailUser => {
        var user = new SocailUsers;
        user.provider = socailUser.provider;
        user.email = socailUser.email;
        user.name = socailUser.name;
        this.SaveResponse(user);
      }, (closed: any) => {
        console.log('User cancelled login or did not fully authorize.');
      }).catch((err: any) => {
        console.log(err);
      });
    }
    catch { }
  }

  SaveResponse(socialUser: SocailUsers) {
    console.log(socialUser);
    this.loginServices.saveResponse(socialUser).subscribe(
      (res:any) => {
        if(res.message=="Email does not exist"){
          this.loginServices.email = socialUser.email;
          this.router.navigate(['/createSocail',  socialUser.email]);
        }
        if(res.message=="Lam viec voi token"){
          localStorage.setItem('token', res.token);
          this.router.navigateByUrl('/management')        
        }
        // this.socailUsers = res as any;
        // localStorage.setItem('socialusers', JSON.stringify(socialUser));
        // this.router.navigateByUrl('/');
      },
      err => {
        console.log(err);
      })
  }


  // goToForgotPassword() {
  //   this.router.navigateByUrl('/user/forgotPassword')
  // }

  onSubmit(form: NgForm) {
    this.loginServices.login(form.value).subscribe(
      (res: any) => {
        if(res.success!=false)
        {
        localStorage.setItem('token', res.token);
        this.router.navigateByUrl('/management')
        }
        else {
          this.toastr.info('Email hay mật khẩu không đúng', 'Đăng nhập thất bại');
        }
      },
      err => {
          console.log(err);
      }
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSocailComponent } from './create-socail.component';

describe('CreateSocailComponent', () => {
  let component: CreateSocailComponent;
  let fixture: ComponentFixture<CreateSocailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSocailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSocailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

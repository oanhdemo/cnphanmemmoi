import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { loginService } from '../services/loginService.service';

@Component({
  selector: 'app-create-socail',
  templateUrl: './create-socail.component.html',
  styleUrls: ['./create-socail.component.scss']
})
export class CreateSocailComponent implements OnInit {

  constructor(
    public service: loginService,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
  }
  onSubmit() {
    this.service.formModel.value.Email = this.service.email;
    this.service.createUser().subscribe(
      (res: any) => {
        if (res.message == "Create Successful") {
          this.toastr.success('New user created', 'Registration successful');
          var socialUser = {
            email: this.service.formModel.value.Email,
          }
          this.service.saveResponse(socialUser).subscribe(
            (res: any) => {
              if (res.message == "Lam viec voi token") {
                localStorage.setItem('token', res.token);
                this.router.navigateByUrl('/management')
              }
            },
            err => {
              console.log(err);
            })
          this.service.formModel.reset();
        }
        else {
          this.toastr.error(res.message, 'Registration failed')
        }
      },
      err => {
        console.log(err)
      }
    );
  }
}
